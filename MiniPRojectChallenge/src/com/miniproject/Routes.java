package com.miniproject;

public class Routes {
private String From;
private String To;
private int distance;
private String travelTime;
private String ticketFare;

    public Routes() {
    }

    public Routes(String from, String to, int distance, String travelTime, String ticketFare) {
        From = from;
        To = to;
        this.distance = distance;
        this.travelTime = travelTime;
        this.ticketFare = ticketFare;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public String getTo() {
        return To;
    }

    public void setTo(String to) {
        To = to;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public String getTicketFare() {
        return ticketFare;
    }

    public void setTicketFare(String ticketFare) {
        this.ticketFare = ticketFare;
    }

    @Override
    public String toString() {

        return this.From+"\t"+this.To+"\t"+this.distance+"\t"+this.travelTime+"\t"
                +this.ticketFare;
    }
}
