package com.miniproject;

import java.util.Scanner;

public class RoutePlannerImpl {
    public static void main(String args[])
    {
        RoutePlanner routePlanner=new RoutePlanner();
        Routes routes[]=routePlanner.readFromFile("routes.csv");
        System.out.println("-------------FLIGHT ROUTE INFORMATION-----------");
        System.out.println("From\tTo\tDistance\tTimeTravel\tAirFare");
        for(int i=0;i<routes.length;i++){
            System.out.println(routes[i]);
        }
        Scanner sc=new Scanner(System.in);
        System.out.println();
        System.out.println();
        System.out.println("-------------SEARCH FLIGHTS-------------");
        System.out.println("Enter from which city you want to fly: ");
        String fromCity=sc.nextLine();
        routePlanner.showDirectFlights(routes, fromCity);
        System.out.println();
        System.out.println("-------------SEARCH AND INTERMEDIATE FLIGHTS-------------");
        System.out.println("Enter Source ");
        String fromCityy = sc.nextLine();
        System.out.println("Enter Destination");
        String toCity = sc.nextLine();
        routePlanner.showAllConnections(routes,fromCityy,toCity);
    }
}
