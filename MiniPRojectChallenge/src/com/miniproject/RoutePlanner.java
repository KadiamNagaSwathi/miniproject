package com.miniproject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.*;
import java.util.Collections;
import java.util.Locale;

public class RoutePlanner {
    static int check=0;
    public static int  readLineCount(String filename)
    {
        int count =-1;
        try(BufferedReader br=new BufferedReader(new FileReader(filename)))
        {
            while(br.readLine()!=null)
            {
            count=count+1;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return count;
    }
    public Routes[] readFromFile(String filename)
    {
        int count=readLineCount(filename);
        Routes[] routes=new Routes[count];
        int index=0;
        try(BufferedReader br=new BufferedReader(new FileReader(filename)))
        {
            String line=null;
            br.readLine();
            while((line=br.readLine())!=null)
            {
                String s[]=line.split(",");
               Routes r=new Routes();
               r.setFrom(s[0]);
               r.setTo(s[1]);
               r.setDistance(Integer.parseInt(s[2]));
               r.setTravelTime(s[3]);
               r.setTicketFare(s[4]);
               routes[index]=r;
                       index++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return routes;
    }
    void showDirectFlights(Routes[] routeInfo,String fromCity)
    {
        int count=0;
        for(int i=0;i<routeInfo.length;i++)
        {
            if((routeInfo[i].getFrom().toLowerCase()).equals(fromCity.toLowerCase()))
            {
                count++;
            }
        }
        if(count==0)
        {
            System.out.println("We are sorry. At this point of time, we do not" +
                    " have any information on flights originating from "+fromCity+".");
        }
        else
        {
            Routes[] directFlights=new Routes[count];
            int index=0;
            for(int i=0;i<routeInfo.length;i++)
            {
                if ((routeInfo[i].getFrom().toLowerCase()).equals(fromCity.toLowerCase())) {
                    if (index == 0) {
                        System.out.println("From\tTo\tDistance\tTimeTravel\tAirFare");
                    }
                    directFlights[index]=new Routes(routeInfo[i].getFrom(),routeInfo[i].getTo(),
                            routeInfo[i].getDistance(),routeInfo[i].getTravelTime(),routeInfo[i].getTicketFare());
                            index++;
                }
            }
            sortDirectFlights(directFlights);
        }


    }
    void sortDirectFlights(Routes[] directFlights)
    {
        Arrays.sort(directFlights,new DirectFlightSort());
        for(int i=0;i<directFlights.length;i++) {
            System.out.println(directFlights[i]);
        }
    }
    public  void showAllConnections(Routes[] routeInfo,String fromCity,String toCity)
    {

        for(int i=0;i<routeInfo.length;i++)
        {
            if(routeInfo[i].getFrom().toLowerCase().equals(fromCity.toLowerCase()) && routeInfo[i].getTo().toLowerCase().equals(toCity.toLowerCase()))
            {
                if(check==0)
                    System.out.println("From\tTo\tDistance\tTimeTravel\tAirFare");
                System.out.println(routeInfo[i]);
                check+=1;

            }
        }
        List<Routes> r=new ArrayList<>();
       check=check+intermediateConnections(routeInfo,fromCity,toCity,check,r);
       if(check==0)
           System.out.println("Sorry, Currently no flights available between: "+ fromCity +" and "+toCity);
    }
   public  static int intermediateConnections(Routes[] routeInfo,String fromCity,String toCity,int check, List<Routes> r)
   {

       for(int i=0;i<routeInfo.length;i++)
       {

           if(routeInfo[i].getFrom().equalsIgnoreCase(fromCity))
        {

            if(!routeInfo[i].getTo().equalsIgnoreCase(toCity))
            {
                r.add(routeInfo[i]);
                intermediateConnections(routeInfo,routeInfo[i].getTo(),toCity,check,r);
                r=null;
                r=new ArrayList<>();
            }
            else if(r.size()!=0){
                for(int j=0;j<r.size();j++)
                {
                    Routes p=r.get(j);
                    System.out.println(p);
                }
                System.out.println(routeInfo[i]);
                    check=check+1;
            }
        }
       }

       return check;
   }

}
